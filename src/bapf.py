#BAPF Universal v0.015
import argparse
import requests
from dataclasses import dataclass

@dataclass
class Wrapper:
    uri: str
    status: int

def InitializeParser():
    parser = argparse.ArgumentParser(description = "Basic Admin Panel Finder. \nURI Bruteforcer")
    parser.add_argument("-u", "--uri", type = str, help = "specifies uri to bruteforce")
    parser.add_argument("-p", "--path", type = str, help = "specifies dictionary path")
    return parser

def Request(uri):
    request = requests.get(uri)
    return request.status_code

def Extract(path, separator):
    file = open(path, "r", encoding = "UTF-8")
    temp = file.read()
    file.close()
    content = temp.split(separator)
    return content

def Force(dictionary, uri, cancellationToken):
    for i in dictionary:
        wrapper = Wrapper("", 0)
        wrapper.uri = uri + i
        wrapper.status = Request(wrapper.uri)
        yield wrapper
        if wrapper.status == cancellationToken:
            break

def main():
    parser = InitializeParser()
    args = parser.parse_args()
    dictionary = Extract(args.path, "\n")
    result = Force(dictionary, args.uri, 200)
    for i in result:
        print(i.uri, i.status)

if __name__ == "__main__":
    main()
