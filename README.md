# BAPF-Universal  
## Description:  
**Basic Admin Panel Finder universal version, written in Python.**  
**BAPF states for "Basic Admin Panel Finder".**  
**It isn't the main tool, when attacking sites.**  
**You can find admin panel, using this tool and then try to bruteforce it.**  
**(using Hydra for e.g.)**  

## Usage:  
**BAPF Universal has console options, that you must specify before executing.**  
> **-h, --help : shows the help message (optional)**  
> **-u, --uri : option expects you to specify victim's uri**  
> **-p, --path : option expects you to specify dictionary path**  

**The program will simply try bruteforcing the most common variants, listed in its dictionary.**

## Warning:
**Program or its owners/developers do not call for any illegal actions**  
**Use it on a site only if you have owner's permission.**